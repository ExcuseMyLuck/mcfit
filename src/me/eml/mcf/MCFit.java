package me.eml.mcf;

import java.util.Calendar;
import java.util.HashMap;

import me.eml.mcf.events.onPlayerJoin;
import me.eml.mcf.events.onPlayerLeave;
import me.eml.mcf.utils.LogManager;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;

public class MCFit extends JavaPlugin {
	private LogManager logger = new LogManager(this);
	public HashMap<String, Integer> playerJoinTime = new HashMap<String, Integer>();
	public String chatPrefix = ChatColor.BLUE + "[MCFit] " + ChatColor.RESET;

	@Override
	public void onEnable() {
		getServer().getPluginManager().registerEvents(new onPlayerJoin(this),
				this);
		getServer().getPluginManager().registerEvents(new onPlayerLeave(this),
				this);

		new BukkitRunnable() {
			@SuppressWarnings("deprecation")
			@Override
			public void run() {
				for (String playerName : playerJoinTime.keySet()) {
					int timePlayerJoined = playerJoinTime.get(playerName);

					if (Calendar.getInstance().get(Calendar.MINUTE) == timePlayerJoined) {
						Player player = getServer().getPlayer(playerName);

						player.sendMessage(chatPrefix
								+ "This is your hourly reminder to be active and do some of the following things: stand up, move in place, and stretch your body out to keep yourself loose.");
					}
				}
			}
		}.runTaskTimer(this, 0L, 1200L);

		logger.logEnable();
	}

	@Override
	public void onDisable() {
		logger.logDisable();
	}
}
