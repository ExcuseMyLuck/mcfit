package me.eml.mcf.events;

import java.util.Calendar;

import me.eml.mcf.MCFit;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class onPlayerJoin implements Listener {
	private MCFit mcfit;

	public onPlayerJoin(MCFit mcfit) {
		this.mcfit = mcfit;
	}

	@EventHandler
	public void playerJoin(PlayerJoinEvent e) {
		mcfit.playerJoinTime.put(e.getPlayer().getName(), Calendar
				.getInstance().get(Calendar.MINUTE));
		e.getPlayer()
				.sendMessage(
						mcfit.chatPrefix
								+ "This server is using MCFit to help their players stay or become healthy!");
		e.getPlayer()
				.sendMessage(
						mcfit.chatPrefix
								+ "Please remember to play in a comfortable position and take breaks from the game at regular intervals.");
	}
}
