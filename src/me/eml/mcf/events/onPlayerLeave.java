package me.eml.mcf.events;

import me.eml.mcf.MCFit;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

public class onPlayerLeave implements Listener {
	private MCFit mcfit;

	public onPlayerLeave(MCFit mcfit) {
		this.mcfit = mcfit;
	}

	@EventHandler
	public void playerLeave(PlayerQuitEvent e) {
		mcfit.playerJoinTime.remove(e.getPlayer().getName());
	}
}
